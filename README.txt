mkmisc - make misc device nodes with dynamic minor number

Copyright (C) 2011  Antonio Ospite <ospite@studenti.unina.it>

A misc device is a character device node which can have a dynamic minor
number, see:
http://www.linux.it/~rubini/docs/misc/misc.html
