CFLAGS=-ansi -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS+=-fno-common \
-Wall \
-Wdeclaration-after-statement \
-Wextra \
-Wformat=2 \
-Winit-self \
-Winline \
-Wpacked \
-Wp,-D_FORTIFY_SOURCE=2 \
-Wpointer-arith \
-Wlarger-than-65500 \
-Wmissing-declarations \
-Wmissing-format-attribute \
-Wmissing-noreturn \
-Wmissing-prototypes \
-Wnested-externs \
-Wold-style-definition \
-Wredundant-decls \
-Wsign-compare \
-Wstrict-aliasing=2 \
-Wstrict-prototypes \
-Wswitch-enum \
-Wundef \
-Wunreachable-code \
-Wunsafe-loop-optimizations \
-Wwrite-strings

CFLAGS += -D_BSD_SOURCE -D_XOPEN_SOURCE

mkmisc: mkmisc.o

clean:
	-rm -f *~ *.o mkmisc
